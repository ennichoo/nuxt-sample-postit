import type { UseFetchOptions } from '#app'
import { defu } from 'defu'
import type { CacheOptions, FetchResponseType } from '~/types/Fetch'

export async function useApiFetch<T> (url: string, options: UseFetchOptions<FetchResponseType<T>> & CacheOptions = {}) {
    // Default cache options
    options.enableCache ??= false
    options.cacheDuration ??= undefined
    options.invalidateCache ??= false

    // Nuxt app instance
    const nuxtApp = useNuxtApp()

    // Default options for useFetch
    const defaults: UseFetchOptions<FetchResponseType<T>> = {
        transform: (response) => {
            return {
                ...response,
                fetchedAt: new Date()
            }
        }
        /**
         * NOTE: There is a bug here, use with caution
         * https://github.com/nuxt/nuxt/issues/27200
        */
        // getCachedData: (key) => {
        //     // Cache is disabled, always get new request
        //     if (!options.enableCache || options.invalidateCache) { return }

        //     const cachedData = nuxtApp.payload.data[key] || nuxtApp.static.data[key]

        //     // Perform new fetch
        //     if (!cachedData) {
        //         return
        //     }

        //     // Check if cacheDuration is not set
        //     if (options.cacheDuration === undefined) {
        //         return cachedData
        //     }

        //     // Check if cache is expired
        //     if (options.cacheDuration) {

        //         const expirationDate = new Date(cachedData.fetchedAt)
        //         expirationDate.setTime(expirationDate.getTime() + options.cacheDuration)

        //         const isExpired = expirationDate < new Date()

        //         if (isExpired) {
        //             return
        //         }
        //     }

        //     return cachedData
        // }
    }

    const params = defu(options, defaults)

    return useFetch(url, params)
}
