import { defineStore } from 'pinia';
import { getUsers, postLogin, postLogout } from '~/constants/api-routes';
import type { User } from '~/types/User';
// eslint-disable-next-line import/order
import { useAllUsersStore } from './allUsers';

export const useUserStore = defineStore('user', () => {
    const { setAllUsers, resetAllUsers } = useAllUsersStore()

    /**
     * State
     */
    const profile = ref<User>();

    /**
     * Actions
     */
    const login = async (override = true) => {

        // If profile is already set and override is false, do not fetch
        if (profile.value && !override) {
            return
        }

        // Fetch profile
        const res = await useApiFetch<User[]>(postLogin, {
            method: 'POST'
        })

        // Set profile (retrieve first user from sample API)
        if (res.data.value?.OK) {
            profile.value = (res.data.value?.data?.[0])
        }

        // store all profiles (for demo purposes)
        setAllUsers(res.data.value?.data ?? [])
    }

    const fetchSelf = async () => {
        const res = await useApiFetch<User[]>(getUsers)

        // Set profile (retrieve first user from sample API)
        if (res.data.value?.OK) {
            profile.value = (res.data.value?.data?.[0])
        }

        // store all profiles (for demo purposes)
        setAllUsers(res.data.value?.data ?? [])
    }

    const logout = async () => {
        const res = await useApiFetch<object>(postLogout, {
            method: 'POST'
        })

        if (res.data.value?.OK) {
            // clear data
            profile.value = undefined
            resetAllUsers()
        }
    }

    /**
     * Getters
     */

    const isLoggedIn = computed(() => {
        return !!profile.value
    })

    return {
        profile,
        isLoggedIn,
        fetchSelf,
        login,
        logout
    }

})
