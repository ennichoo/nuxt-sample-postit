import { defineStore } from 'pinia'

export const useMyCounterStore = defineStore('myCounter', () => {

    /**
     * State
     */
    const counter = ref(0)

    /**
     * Actions
     */
    const increment = () => {
        counter.value++
    }

    /**
     * Getters
     */
    const doubleCounter = computed(() => counter.value * 2)

    return {
        counter,
        increment,
        doubleCounter
    }
})
