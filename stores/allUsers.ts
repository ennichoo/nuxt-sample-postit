import { defineStore } from 'pinia';
import type { User } from '~/types/User';

export const useAllUsersStore = defineStore('allUsersStore', () => {
    /**
     * State
     */

    const allUsers = ref<User[]>([]);

    /**
     * Actions
     */
    const setAllUsers = (users: User[]) => {
        allUsers.value = users
    }

    const resetAllUsers = () => {
        allUsers.value = []
    }

    const getUser = (userId: number) => {
        const user = allUsers.value.find(user => user.id === userId)
        return user
    }

    return {
        allUsers,
        getUser,
        setAllUsers,
        resetAllUsers
    }
})
