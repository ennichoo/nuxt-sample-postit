import { redirectToHome } from '~/utils/redirect'

export default defineNuxtRouteMiddleware(() => {

    // Get Cookie
    const isLoggedIn = useCookie('isLoggedIn')

    if (isLoggedIn.value) {
        return redirectToHome()
    }
})
