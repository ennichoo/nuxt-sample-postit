# CHANGE LOG

## 1.0.3 (2024-2-19)

### 🔥 Feature
- Add plugin to access env

### 🏡 Chore

- Update README for shadcn
- Remove env config file

## 1.0.2 (2024-2-14)

### 🏡 Chore

- Change NuxtUI to Shadcn-Vue
- Update README for shadcn
- Bump dependencies

## 1.0.1 (2023-11-07)

### 🏡 Chore
- Remove nuxt-security module

### 🩹 Bugfix
- Update missing implicit type imports

--- 

## 1.0.0 (2023-10-23)

### 🏡 Chore
- Bump dependency of Nuxt to 2.8.0
- Update to set type imports to be implicit

### 🔥 Feature
- Composable: Implement enableCache and cacheDuration to useApiFetch for Nuxt [getCacheData](https://nuxt.com/blog/v3-8#%EF%B8%8F-data-fetching-improvements)
