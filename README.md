
# Versions

| No |Item  | Version |
|--|--|--|
| 1 | Nuxt | 3.10.1
| 2 | Bun (optional) | 1.1.0
| 3 | PNPM | 8.15.7
| 4 | Node|20.5.0

# Steps to run this project locally

1. Copy paste the `.env.example` file and rename it to `.env`

2. Paste the following in your `.env` file:
```
NUXT_PUBLIC_APP_NAME=PostIt
NUXT_PUBLIC_SITE_URL=
NUXT_API_BASE_URL=https://jsonplaceholder.typicode.com
NUXT_PUBLIC_SERVER_ENV=production
```

2. Run your install command.
```
pnpm install
```

3. Start your local server.
```
pnpm run dev
```

# API
This project utilizes the [Typicode APIs](https://jsonplaceholder.typicode.com/) to fetch and manage sample data. Refer to the documentation provided in the link for more information.
