export const postLogin = '/api/login'
export const postLogout = '/api/logout'

// sample APIs are from here: https://jsonplaceholder.typicode.com
// note that the apis do not have a '/api' prefix but it is required here
// for nuxt's catch-all server routes to work
export const getUsers = '/api/users'
export const getPosts = '/api/posts'
export const getPost = (id: string) => `/api/posts/${id}`
export const getPostComments = (id: string) => `/api/posts/${id}/comments`

export const createPost = '/api/posts'
export const createComment = (postId: string) => `/api/posts/${postId}/comments`