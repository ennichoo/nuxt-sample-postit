export default defineNuxtPlugin((nuxtApp) => {
    const serverEnv = nuxtApp.$config.public.serverEnv

    return {
        provide: {
            serverEnv,
            isProduction: serverEnv === 'production'
        }
    }
})
