// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
    devtools: { enabled: true },
    experimental: { typedPages: true },
    typescript: {
        shim: false
    },
    modules: [
        '@pinia/nuxt',
        '@nuxtjs/tailwindcss'
    ],

    // Globally available css
    css: ['~/assets/scss/main.scss'],
    runtimeConfig: {
        apiBaseUrl: process.env.NUXT_API_BASE_URL,
        public: {
            appName: process.env.NUXT_PUBLIC_APP_NAME,
            serverEnv: process.env.NUXT_PUBLIC_SERVER_ENV
        }
    },
    components: [
        {
            path: '~/components/ui',
            prefix: 'ui',
            extensions: ['vue']
        },
        {
            path: '~/components',
            extensions: ['vue']
        }
    ]
})
