// export type BackendFetchResponseType<T> = {
//     error: boolean,
//     errorCode: number,
//     userMessage: string,
//     devMessage: string | null,
//     response: T
// }

export type FetchResponseType<T> = {
    OK: boolean,
    data?: T
    fetchedAt?: Date
}

export type CacheOptions = {
    enableCache?: boolean,
    cacheDuration?: number,
    invalidateCache?: boolean
}
