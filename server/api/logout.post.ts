export default defineEventHandler((event) => {
    // Logout API goes here

    // clear token
    deleteCookie(event, 'isLoggedIn', {
        path: '/'
    })

    return true
})
