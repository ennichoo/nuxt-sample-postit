import { getUsers } from '~/constants/api-routes';
import { User } from '~/types/User';

export default defineEventHandler(async (event) => {
    // Login API goes here
    const res = await useExternalFetch<User[]>(getUsers, {
        axiosConfig: {
            method: 'GET'
        }
    })

    if (res.OK) {
        // set token and other user data here
        setCookie(event, 'isLoggedIn', 'true', {
            path: '/'
        })
    }

    return res
})