export default defineEventHandler(async (event) => {

    // Get Cookie Token
    const token = getCookie(event, 'token')

    // GET Method
    try {
        if (event.node.req.method === 'GET') {
            const query = getQuery(event)

            const res = await useExternalFetch(String(event.path), {
                token: String(token),
                axiosConfig: {
                    method: 'GET',
                    params: query
                }
            })

            return res;
        }

        // POST Method
        if (event.node.req.method === 'POST') {
            const body = await readBody(event);

            const res = await useExternalFetch(String(event.path), {
                token: String(token),
                axiosConfig: {
                    method: 'POST',
                    data: body
                }
            })

            return res
        }
    } catch (error) {
        console.log('error');
        console.log(error);

    }
})
