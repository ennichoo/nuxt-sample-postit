import axios, { AxiosError, AxiosRequestConfig } from 'axios';
import type { FetchResponseType } from '~/types/Fetch';

const config = useRuntimeConfig()

const isAxiosError = (error: any): error is AxiosError => {
    return (error as AxiosError).isAxiosError !== undefined;
}

/**
 * Combine string
 */
export const externalAPIRoute = (path: string) => {
    // remove '/api' from path - for typicode to work
    path = path.replace('/api', '')
    return `${config.apiBaseUrl}${path}`
}

/**
 * Use this only for external API calls in server route
 */
export const useExternalFetch = async <T>(
    url: string,
    options: {
        token ?: string | boolean | undefined,
        axiosConfig?: AxiosRequestConfig,
        removeContentTypeHeader?: boolean
    }
): Promise<FetchResponseType<T>> => {

    // Base response
    const baseResponse = {
        OK: true
    } as FetchResponseType<T>

    try {
        const apiCall = await axios(externalAPIRoute(url), {
            ...options.axiosConfig,
            headers: {
                Accept: 'application/json',
                'Accept-Language': 'en',
                ...(options.removeContentTypeHeader ? {} : { 'Content-Type': 'application/json' }),
                Authorization: `Bearer ${options.token}`
            }
        })

        if (apiCall.status >= 200 && apiCall.status <= 299) {
            return {
                ...baseResponse,
                data: apiCall.data as T
            }
        } else {
            return {
                ...baseResponse,
                OK: false,
                data: apiCall.data as T
            }
        }
    } catch (e) {
        if (isAxiosError(e) && e.response) {
            return {
                ...baseResponse,
                OK: false,
                data: e.response.data as T
            };
        }

        // If the error was not due to an HTTP response, return a generic error message
        return {
            OK: false
        };
    }
}
