const generateRandomAvatar = () => {
    const firstname = getRandomAlphabet();
    const lastname = getRandomAlphabet();
    return `https://avatar.iran.liara.run/username?username=${firstname}+${lastname}`
}

const getRandomAlphabet = () => {
    const alphabets = 'abcdefghijklmnopqrstuvwxyz';
    const randomIndex = Math.floor(Math.random() * alphabets.length);
    return alphabets[randomIndex];
}

export default generateRandomAvatar