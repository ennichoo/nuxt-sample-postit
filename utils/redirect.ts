export const redirectToLogin = () => navigateTo('/login');
export const redirectToHome = () => navigateTo('/');