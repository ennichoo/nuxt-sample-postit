#!/bin/bash

# Create .env file with environment variables
cat > .env <<EOL
API_BASE_URL=$API_BASE_URL_DEV
EOL

# Clean install dependencies
npm ci

# Build the project
CI=false npm run build

