#!/bin/bash

# Transfer files to the remote server using rsync
rsync -avz --delete --exclude '.git' .nuxt .output public package.json nuxt.config.ts .env ubuntu@$DEV_IP:your-project-folder

# SSH into the remote server and reload the PM2 process
ssh ubuntu@$DEV_IP "cd ./your-project-folder && sudo pm2 reload ecosystem.config.js && exit"

